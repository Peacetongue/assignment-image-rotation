//
// Created by Paul on 30.12.2021.
//
#include "../Libs/BMP.h"



static struct bmp_header bmp_header_create(uint64_t width, uint64_t height){
    struct bmp_header new_bmp = {
            .bfType = 0x4D42,
            .bfileSize = (uint32_t)((width * sizeof(struct pixel) + ((4 - (sizeof(struct pixel) * width) % 4) % 4)) * height + sizeof(struct bmp_header)),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = (uint32_t)width,
            .biHeight = (uint32_t)height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = (uint32_t)((width * sizeof(struct pixel) + ((4 - (sizeof(struct pixel) * width) % 4) % 4)) * height),
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
    return new_bmp;
}

enum read_status from_bmp( FILE* in, struct image* img ) {
    struct bmp_header header = {0};
    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) {
        return READ_INVALID_HEADER;
    }
    if (header.bfType != 0x4d42) {
        return READ_INVALID_SIGNATURE;
    }
    if (header.biBitCount != 24) {
        return READ_INVALID_BITS;
    }
    *img = create_img(header.biWidth, header.biHeight);
    uint32_t padding = (4 - (sizeof(struct pixel) * img->width) % 4) % 4;
    uint32_t k = 0;
    for (uint64_t i = 0; i < img->height; i++) {
        for (uint64_t j = 0; j < img->width; j++) {
            fread(img->data + k, sizeof(struct pixel), 1, in);
            k = k + 1;
        }
        fseek(in, padding, SEEK_CUR);
    }
    delete_img(img);
    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ){
    struct bmp_header header = bmp_header_create(img->width, img->height);
    struct bmp_header* pointer_header = &header;
    if (fwrite(pointer_header, sizeof(struct  bmp_header), 1, out) != 1) {
        return WRITE_ERROR;
    }
    const uint32_t padding = (4 - (sizeof(struct pixel) * img->width) % 4) % 4;
    uint64_t k = 0;
    for(uint32_t i=0; i<img->height; i++) {
        fwrite(img->data+i*img->width, sizeof(struct pixel)*img->width, 1, out);
        fwrite(&k, 1, padding, out);
    }
    return WRITE_OK;
}
