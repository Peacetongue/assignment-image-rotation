//
// Created by Paul on 30.12.2021.
//
#include "../Libs/image.h"
#include <mm_malloc.h>


struct image create_img(uint64_t width, uint64_t height){
    struct image result =  {
            result.width = width,
            result.height = height,
            result.data = malloc(width * height * sizeof(struct pixel))
    };
    return result;
}
void delete_img(struct image* img){
    free(img -> data);
}
