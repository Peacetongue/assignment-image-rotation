
#include "../Libs/rotate.h"
#include "../Libs/BMP.h"

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning

    if (argc != 3) {
        fprintf(stderr, "Wrong number of arguments.");
        return 1;
    }

    FILE* in = fopen( argv[1], "rb");
    if (!in) {
        fprintf(stderr, "Error while open %s", argv[1]);
        return 1;
    }

    FILE* out = fopen(argv[2], "w");

    if (!out) {
        fprintf(stderr, "Error while open %s", argv[2]);
        return 1;
    }

    struct image image = {0};
    enum read_status read_status = from_bmp(in, &image);
    if (read_status != READ_OK) {
        printf("Error while reading: %d\n", read_status);
        fprintf(stderr, "Error while reading: %d\n", read_status);
        fclose(in);
        fclose(out);
        return 1;
    }


    from_bmp( in, &image);

    fclose(in);

    struct image result = rotate( &image );

    if (to_bmp(out, &result) != WRITE_OK) {
        fclose(in);
        fclose(out);
        delete_img(&image);
        delete_img(&result);
        fprintf(stderr, "Error while writing\n");
        return 1;
    }

    delete_img(&image);

    to_bmp(out, &result);

    delete_img(&result);

    fclose(out);

    return 0;
}
