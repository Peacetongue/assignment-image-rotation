//
// Created by Paul on 30.12.2021.
//
#include "../Libs/image.h"
#include "../Libs/rotate.h"

struct image rotate( struct image const* source ){
    struct image result = create_img(source->height, source->width);
    uint32_t k = 0;
    for (int64_t i = result.width - 1; i >= 0; i--) {
        for (int64_t j = 0; j < result.height; j++) {
            result.data[j * result.width + i] = source->data[k];
            k = k + 1;
        }
    }
    return result;
}
