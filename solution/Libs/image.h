//
// Created by Paul on 29.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#include <stdint.h>
#include <stdio.h>

struct __attribute__((packed)) pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};
struct image create_img(
        uint64_t width,
        uint64_t height);

void delete_img(struct image* img);


#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
