//
// Created by Paul on 30.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATE_H

/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate( struct image const* source );


#endif //ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
